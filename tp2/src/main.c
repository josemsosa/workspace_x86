/*==================[inclusions]=============================================*/

#include "main.h"
#include "hw.h"
#include "fsmCafe.h"




/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input = 0;
    uint16_t cont_ms = 0;

    hw_Init();
    fsmCafeInit();


    // Superloop hasta que se presione la tecla Esc
    while (input != EXIT) {
        input = hw_LeerEntrada();

        if (input == SENSOR_1) {
            fsmCafe_Raise_evIngresoFicha();
        }

        if (input == SENSOR_2) {
            fsmCafe_Raise_evSeleccionTe();
        }
        
        if (input == SENSOR_3)
        {
            fsmCafe_Raise_evSeleccionCafe();        
        }
        
        
        cont_ms++;
        if (cont_ms == 1000)
        {
            cont_ms = 0;
            fsmCafe_Raise_evTickms();
        }
        
        fsmCafeEvaluacion();

        hw_Pausems(1);
    }

    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/
