/*==================[inclusions]=============================================*/

#include "hw.h"
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

struct termios oldt, newt;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

void hw_Init(void)
{
    // Configurar la terminal para evitar presionar Enter usando getchar()
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);       
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);

    // Non-blocking input
    fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);
}

void hw_DeInit(void)
{
    // Restaurar la configuracion original de la terminal
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);    
}

void hw_Pausems(uint16_t t)
{
    usleep(t * 1000);
}

uint8_t hw_LeerEntrada(void)
{
    return getchar();
}

// Indicadores de entrada y salida de ficha.
void hw_IngresoFicha()
{
    printf("Ficha ingresada. \n\n");
}
void hw_SalidaFicha()
{
    printf("Time out. Ficha saliendo. \n\n");
}

// Indicadores de erogaciones. 
void hw_ErogacionTe()
{
    printf("Erogacion de te. \n\n");
}
void hw_ErogacionCafe()
{
    printf("Erogacion de cafe. \n\n");
}

// Indicadores de estados de la alarma.
void hw_ActivarAlarma()
{
    printf("Alarma activada \n\n");
}
void hw_ApagarAlarma()
{
    printf("Alarma apagada \n\n");
}

// indicador de Cafetera en funcionamiento.
void hw_IndicadorFuncionamiento()
{
    printf("Cafetera en funcionamiento.\n\n");
}

/*==================[end of file]============================================*/
