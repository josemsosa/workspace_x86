#include <stdbool.h>
#include <stdio.h>
#include "hw.h"
#include "fsmCafe.h"

#define COUNT_FICHA 30
#define COUNT_EROGACION 5
#define COUNT_ALARM 2
#define COUNT_EROGACION_TE 6
#define COUNT_EROGACION_CAFE 9

static FSM_CAFE_STATES state;

static bool evTickms;
static bool evSeleccionTe;
static bool evSeleccionCafe;
static bool evIngresoFicha;

static __uint8_t count;
static __uint8_t countaux;


static void clearEvents()
{
    evTickms = 0;
    evSeleccionTe = 0;
    evSeleccionCafe = 0;
    evIngresoFicha = 0;    
}

void fsmCafeInit()
{
    state = REPOSO;
    clearEvents();
}

void fsmCafeEvaluacion ()
{
    switch (state)
    {
    case REPOSO:
        if(evTickms)
        {
            hw_IndicadorFuncionamiento();
        }else if (evIngresoFicha)
        {
            count = 0;
            hw_IngresoFicha();
            state = INGRESO_FICHA;
        }     
        break;

    case INGRESO_FICHA:
        if (evTickms && count < COUNT_FICHA)
        {
            count++;        
        }else if (evTickms && count == COUNT_FICHA)
        {
            count = 0 ;
            hw_SalidaFicha();
            state = REPOSO;
        }else if (evSeleccionTe && count < COUNT_FICHA)
        {
            count = 0;
            countaux = 0;
            hw_ErogacionTe();
            state = EROGACION_TE;
        }else if (evSeleccionCafe && count < COUNT_FICHA)
        {
            count = 0;
            countaux = 0;
            hw_ErogacionCafe();
            state = EROGACION_CAFE;
        }              
        break;

    case EROGACION_TE:
        if (evTickms && count < COUNT_EROGACION && countaux < COUNT_EROGACION_TE)
        {
            count++;
        }else if (evTickms && count == COUNT_EROGACION)
        {
            count = 0;
            countaux++;
            hw_ErogacionTe();
        }else if (countaux == COUNT_EROGACION_TE)
        {
            count = 0;
            hw_ActivarAlarma();
            state = ALARMA;
        }
        break;
    case EROGACION_CAFE:
        if (evTickms && count < COUNT_EROGACION && countaux < COUNT_EROGACION_CAFE)
        {
            count++;
        }else if (evTickms && count == COUNT_EROGACION)
        {
            count = 0;
            countaux++;
            hw_ErogacionCafe();
        }else if (countaux == COUNT_EROGACION_CAFE)
        {
            count = 0;
            hw_ActivarAlarma();
            state = ALARMA;
        }      
        break;

    case ALARMA:
        if (evTickms && count < COUNT_ALARM)
        {
            count++;
        }else if (evTickms && count == COUNT_ALARM)
        {
            hw_ApagarAlarma();
            state = REPOSO;
        }        
        break;
    }
    clearEvents();
}


void fsmCafe_Raise_evIngresoFicha()
{
    evIngresoFicha = 1;
}
void fsmCafe_Raise_evSeleccionTe()
{
    evSeleccionTe = 1;    
}
void fsmCafe_Raise_evSeleccionCafe()
{
    evSeleccionCafe = 1;    
}
void fsmCafe_Raise_evTickms()
{
    evTickms = 1;    
}

void fsmCafePrintCurrentState()
{
    printf("Estado actual: %0d \n", state);
}