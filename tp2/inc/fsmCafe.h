#ifndef _FSMCAFE_H_
#define _FSMCAFE_H_

typedef enum {
    REPOSO,
    INGRESO_FICHA,
    EROGACION_TE,
    EROGACION_CAFE,
    ALARMA
} FSM_CAFE_STATES;

// Inicializacion y evaluacion de la FSM
void fsmCafeInit();
void fsmCafeEvaluacion ();
//Eventos
void fsmCafe_Raise_evIngresoFicha();
void fsmCafe_Raise_evSeleccionTe();
void fsmCafe_Raise_evSeleccionCafe();
void fsmCafe_Raise_evTickms();
//Debbug
void fsmCafePrintCurrentState();

#endif /* #ifndef _FSMCAFE_H_ */