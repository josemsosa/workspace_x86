#ifndef _GARAJE_H_
#define _GARAJE_H_ 

typedef enum {
    REPOSO,
    INGRESANDO,
    ESPERANDO_EGRESO,
    ALARMA
} FSM_GARAJE_STATE_T;

//Inicializacion y evaliacion de la FSM 
void fsm_garaje_init();
void fsm_garaje_runCycle();
//Eventos
void fsm_garaje_raise_evSensor1_On();
void fsm_garaje_raise_evSensor2_On();
void fsm_garaje_raise_evSensor2_Off();
void fsm_garaje_raise_evTick1seg();
//Debbug
void fsm_garaje_printCurrentState();

#endif 